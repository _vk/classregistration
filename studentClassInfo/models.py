from django.db import models

class StudentClassInfo(models.Model):
	
	fullname=models.CharField(max_length=100, blank=True,default='')
	mobilephone=models.CharField(max_length=10,default='')
	classname=models.CharField(max_length=100,default='')
	#pin=models.CharField(max_length=4,default='')
