from rest_framework import serializers
from studentClassInfo.models import StudentClassInfo


class StudentClassInfoSerializer(serializers.ModelSerializer):
	class Meta:
		model=StudentClassInfo
		fields=('fullname','mobilephone','classname')