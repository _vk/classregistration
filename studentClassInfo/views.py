from studentClassInfo.models import StudentClassInfo
from studentClassInfo.serializers import StudentClassInfoSerializer
from django.http import Http404,JsonResponse
# from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from twilio.rest import TwilioRestClient
import random
import sqlite3
import json

# from rest_framework import status
from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from snippets.models import Snippet
# from snippets.serializers import SnippetSerializer


@api_view(['GET','POST'])
def studentClassValidation(request,format=None):
	serializer=StudentClassInfoSerializer(data=request.data)
	if request.method == 'GET':	
		db=sqlite3.connect('studentInfo.db')
		db.execute('create table if not exists registration ( fullname text, mobilephone text, classname text)')
		
		res1=db.execute('select * from registration where fullname=?',(request.session.get('fullname'),))
		print(request.session.get('fullname'))
		single_row=res1.fetchone()
		if res1.rowcount>0:
			if single_row['mobilephone']==request.session.get('mobilephone'):
				for row in res1:
					if row['classname']==request.session.get('classname'):
						return Response({'fullname':'exists','mobilephone':'exists','classname':'exists'})
					else:
						return Response({'fullname':'exists','mobilephone':'exists','classname':'!exists'})
			else:
				return Response({'fullname':'exists','mobilephone':'!exists'})
		
		res2=db.execute('select * from registration where mobilephone=?',(request.session.get('mobilephone'),))
		if res2.rowcount>0:
			return Response({'fullname':'!exists','mobilephone':'exists'})

		return Response({'fullname':'!exists','mobilephone':'!exists','classname':'!exists'})

	elif request.method == 'POST':
		serializer=StudentClassInfoSerializer(data=request.data)
		if serializer.is_valid():
			request.session['fullname']=request.data["fullname"]
			request.session['mobilephone']=request.data["mobilephone"]
			request.session['classname']=request.data["classname"]
			print(request.session['fullname'],request.session['mobilephone'],request.session['classname'])
			return Response(serializer.data,status=status.HTTP_201_CREATED)
		return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

			

@api_view(['POST'])
def studentPIN(request,format=None):
	serializer=StudentClassInfoSerializer(data=request.data)
	if serializer.is_valid():
		#Generate PIN
		pin=''
		digits='0123456789'
		for i in range(4):
			pin+=random.choice(digits)
		request.session['pinTemp']=pin

		#Send message to mobile phone
		account_sid="ACaeb5ef46cec244c65356b7e538c0e6dd"
		auth_token="890265a19799f960cf4893173a3b87c8"
		client=TwilioRestClient(account_sid,auth_token)
		message=client.messages.create( 
			to=request.data["mobilephone"],
			from_="+15622691065",body="Account Validation PIN: "+pin)
		print(message.sid)
		return Response(serializer.data,status=status.HTTP_201_CREATED)
	return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','POST'])
def studentClassDetail(request,format=None):
	if request.method == 'GET':	
		studentObject=StudentClassInfo.objects.order_by('-id')
		serializer = StudentClassInfoSerializer(studentObject, many=False)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer=StudentClassInfoSerializer(data=request.data)
		if serializer.is_valid() and request.data["pin"]==request.session['pinTemp']:
			studentObject=serializer.save()
			return Response(serializer.data,status=status.HTTP_201_CREATED)
		return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    

    